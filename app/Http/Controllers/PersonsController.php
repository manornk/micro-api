<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Person;
use App\Address;

class PersonsController extends Controller
{
    //     Funkcionalnosti za implementaciju:
    // a. Dodavanje osobe
    // b. Dodavanje adrese osobi
    // c. Pregled svih osoba
    // d. Pregled jedne osobe
    // e. Azuriranje osobe
    // f. Brisanje osobe

    public function getPersons()
    {
        $persons = Person::with('addresses')->get();

        return response()->json($persons);
    }

    public function getPerson($id)
    {
        $person = Person::find($id);

        if($person == null) 
            return response()->json(['message' => 'Not found']);
            
        return response()->json($person);
    }



    public function addPerson(Request $request)
    {
        $response = array();
        $parameters = $request->json()->all();

        $rules =  array(
            'name'    => 'required',
            'age'    => 'required|numeric'
        );
        $name = $parameters['name'];
        $age = $parameters['age'];

        $messages = array(
            'name.required' => 'name is required.',
            'age.required' => 'age is required.',
            'age.numeric' => 'age must be numeric.'
        );

        $validator = \Validator::make(['name' => $name, 'age' => $age], $rules, $messages);
        if(!$validator->fails()) {
            $response = Person::create($parameters);

            return response()->json(['message' => 'done']);
        } else {
            $errors = $validator->errors();
            return response()->json($errors->all());
        }
    }

    public function addAddress(Request $request, $id)
    {
        $response = array();
        $parameters = $request->json()->all();

        $rules =  array(
            'person_id'    => 'required|numeric',
            'house_number'    => 'required|numeric',
            'street_name'    => 'required',
            'city'    => 'required',
        );
        $person_id = $id;
        $house_number = $parameters['house_number'];
        $street_name = $parameters['street_name'];
        $city = $parameters['city'];
        $parameters['person_id'] = $id;

        $messages = array(
            'name.required' => 'name is required.',
            'age.required' => 'age is required.',
            'house_number.numeric' => 'house_number must be numeric.'
        );

        $validator = \Validator::make(
            [
                'person_id' => $person_id,
                'city' => $city,
                'house_number' => $house_number,
                'street_name' => $street_name,
            ], $rules, $messages);

        if(!$validator->fails()) {
            $response = Address::create($parameters);

            return response()->json(['message' => 'done']);
        } else {
            $errors = $validator->errors();
            return response()->json($errors->all());
        }
    }

    public function updatePerson(Request $request, $id)
    {
        $response = array();
        $parameters = $request->json()->all();

        $rules =  array(
            'name'    => 'required',
            'age'    => 'required|numeric'
        );
        $name = $parameters['name'];
        $age = $parameters['age'];

        $messages = array(
            'name.required' => 'name is required.',
            'age.required' => 'age is required.',
            'age.numeric' => 'age must be numeric.'
        );

        $validator = \Validator::make(['name' => $name, 'age' => $age], $rules, $messages);
        if(!$validator->fails()) {
            $response = Person::create($parameters);

            $response = Person::find($id);
            $response = $response->update($parameters);

            return response()->json(['message' => 'done']);
        } else {
            $errors = $validator->errors();
            return response()->json($errors->all());
        }
    }

    public function deletePerson($id)
    {
        $person = Person::find($id)->delete();
        return response()->json(['message' => 'done']);

    }
}
