<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/getPersons', 'PersonsController@getPersons');
$router->get('/getPerson/{id}', 'PersonsController@getPerson');

$router->post('/addPerson', 'PersonsController@addPerson');
$router->post('/addAddress/{id}', 'PersonsController@addAddress');


$router->put('/updatePerson/{id}', 'PersonsController@updatePerson');
$router->delete('/deletePerson/{id}', 'PersonsController@deletePerson');